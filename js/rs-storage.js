import RemoteStorage from 'https://cdn.skypack.dev/remotestoragejs'

const RsStorage = class extends HTMLElement {
    constructor() {
	super()
    }
    connectedCallback() {
	this.getAttributes()
	this.setupStorage()
	/* this.setupEvents() */
    }

    getAttributes() {
	// config
	this.debug = this.getAttribute('debug') ? true : false
	this.accessClaimDirectory = this.getAttribute('access-claim-directory')
	this.accessClaimRights = this.getAttribute('access-claim-rights')
	this.cachingEnableDirectory = this.getAttribute('caching-enable-directory')
	this.apiKeyDropbrox = this.getAttribute('api-key-dropbrox')
	this.apiKeyGoogledrive = this.getAttribute('api-key-googledrive')

	// events
	this.listenConnected = this.getAttribute('listen-connected') ? true : false
	this.listenDisconnected = this.getAttribute('listen-disconnected') ? true : false
	this.listenNetworkOnline = this.getAttribute('listen-network-online') ? true : false
	this.listenNetworkOffline = this.getAttribute('listen-network-offline') ? true : false
    }

    setupStorage() {
	this.remoteStorage = new RemoteStorage({
	    logging: this.debug
	})
	this.remoteStorage.access.claim(
	    this.accessClaimDirectory,
	    this.accessClaimRights
	)

	if (this.accessClaimRights) {
	    this.remoteStorage.caching.enable(this.cachingEnableDirectory)
	}

	if (this.apiKeyDropbrox || this.apiKeyGoogledrive) {
	    let apiKeys = {}
	    if (this.apiKeyDropbrox) apiKeys['dropbox'] = this.apiKeyDropbrox
	    if (this.apiKeyDropbrox) apiKeys['googledrive'] = this.apiKeyGoogledrive
	    this.remoteStorage.setApiKeys(apiKeys)
	}

	if (!this.storageModule) {
	    this.debug && console.info('remoteStorage', 'No storage module defined')
	} else {
	    this.remoteStorage.addModule(this.storageModule)
	}
	this.debug && console.log('remoteStorage', this.remoteStorage)
    }
    
    setupEvents() {
	if (this.listenConnected) {
	    this.remoteStorage.on('connected', () => {
		const userAddress = this.remoteStorage.remote.userAddress
		this.debug && console.debug(`${userAddress} connected their remote storage.`)
		const remoteStorageConnected = new CustomEvent('remoteStorageConnected', {
		    bubbles: true,
		    detail: 'connected!'
		})

		this.dispatchEvent(remoteStorageConnected)
	    })
	}

	if (this.listenDisconnected) {
	    this.remoteStorage.on('disconnected', () => {
		this.debug && console.debug(`We're disconected.`)
	    })
	}

	if (this.listenNetworkOnline) {
	    this.remoteStorage.on('network-online', () => {
		this.debug && console.debug('network online')
	    })
	}

	if (this.listenNetworkOffline) {
	    this.remoteStorage.on('network-offline', () => {
		this.debug && console.debug(`We're offline now.`)
	    })
	}
    }
}

export default RsStorage
