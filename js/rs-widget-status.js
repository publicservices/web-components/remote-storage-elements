const RsWidget = class extends HTMLElement {
    connectedCallback() {
	this.attributes()
	this.setupWidget()
    }

    disconnectedCallback() {
	/* remove? */
	/* this.remoteStorage.on('connected', this.handleConnected)
	   this.remoteStorage.on('disconnected', this.handleDisonnected) */
    }

    attributes() {
	this.connected = false
	this.setAttribute('title', 'remote-storage is disconnected')
	
	this.debug = this.getAttribute('debug') ? true : false

	this.remoteStorageDOM = document.querySelector('rs-storage')	

	if (!this.remoteStorageDOM) {
	    console.error('Missing <rs-storage> element')
	    return
	} 

	this.remoteStorage = this.remoteStorageDOM.remoteStorage
    }
    
    setupWidget() {
	this.remoteStorage.on('connected', this.handleConnected)
	this.remoteStorage.on('disconnected', this.handleDisonnected)
    }

    handleConnected = () => {
	this.connected = true
	this.setAttribute('connected', true)
	this.setAttribute('title', `remote-storage connected to ${this.remoteStorage.remote.href}`)
    }

    handleDisonnected = () => {
	this.connected = false
	this.removeAttribute('connected')
	this.setAttribute('title', 'remote-storage is disconnected')
    }
}

export default RsWidget
