Attempt to make usefull `remote-storage` HTML/JS custom elements.

Docs:

- [https://remotestoragejs.readthedocs.io/en/v1.2.3/getting-started/how-to-add.html](https://remotestoragejs.readthedocs.io/en/v1.2.3/getting-started/how-to-add.html)


# Usage

Then you can use it like this in your js files"

```
/* import all elements */
import {RsStorage, RsWidget} from './js/index.js'

/* or each elements */
import RsStorage from './js/rs-storage.js'
import RsWidget from './js/rs-widget.js'

/* define elements */
customElements.define('rs-storage', RsStorage)
customElements.define('rs-widget', RsWidget)
```

The **above will not work**, as you will need to define a [storage module](https://remotestoragejs.readthedocs.io/en/latest/data-modules/defining-a-module.html?highlight=module) as in RemoteStorage.js (example in the `./index.html` file).

```
import {
    RsStorage,
    RsWidget
} from 'https://cdn.skypack.dev/remote-storage-elements'

import notesModule from './storage-modules/notes.js'

class NotesStorage extends RsStorage {
    storageModule = notesModule
}

/* define elements */
customElements.define('rs-storage', NotesStorage)
```

You have to nate the storage element `rs-storage`, as the `rs-widget` element will be looking for this HTML DOM element to instanciate (should that be fixed? By which pattern? Cheers).

And like so in your HTML, to instanciate the storage elements:
```
<rs-storage
	hidden=""
	debug="true"
	access-claim-directory="note4000"
	access-claim-rights="rw"
	caching-enable-directory="/note4000/"
	api-key-dropbrox="ibl2lga90i1ubgp"
	api-key-googledrive="77133566319-d76dbmdt9jle773ekplr2k1lrehibt1l.apps.googleusercontent.com"
	listen-connected="true"
	listen-disconnected="true"
	listen-network-online="true"
	listen-network-offline="true"></rs-storage>
```

Optional parameters: `debug`, `hidden`, `caching-enable-directory`, `api-key-*`, `listen-*`.

For the widget element:
```
<rs-widget debug></rs-widget>
```

Optional additionnal parameter: `dettach` parameter (add ="" if not working), to default attaching the widget to the DOM as refault remote storage behavior (not inside the `<rs-widget>` HTML DOM element).
